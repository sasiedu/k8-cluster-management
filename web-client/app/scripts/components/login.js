import React from 'react';
import { connect } from 'react-redux';
import * as FontAwesome from 'react-icons/lib/fa';

class Login extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      username: '',
      password: '',
      login: false
    };

    this.onInputChange = this.onInputChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onInputChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  onSubmit(e) {
    const {username, password } = this.state;

    e.preventDefault();
    if (username === 'read' && password === 'read') {
      localStorage.setItem('user', JSON.stringify({username: 'read'}));
      this.setState({
        username: '',
        password: '',
        login: true
      });
    } else if (username === 'write' && password === 'write') {
      localStorage.setItem('user', JSON.stringify({username: 'write'}));
      this.setState({
        username: '',
        password: '',
        login: true
      });
    } else {
      this.setState({
        username: '',
        password: ''
      });
    }
  }

  loginPageRender(loginErrorMessage) {
    const { username, password } = this.state;

    return (
      <div className="container-fluid login-background">
        <h1>{loginErrorMessage}</h1>
        <div className="strip">
          <div className="animated-container">
            <img alt="helix" src={require('../../images/helix.png')} />
            <div className="copy">
              <h1>HELIX</h1>
              <span>Cluster Management</span>
            </div>
          </div>
          <div className="login-form">
            <form onSubmit={this.onSubmit}>
              <h1 className="active-highlight">LOGIN</h1>
              <div className="form-controls">
                <div className="input-group">
                  <FontAwesome.FaUser className="icon" />
                  <span className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input
                      id="username"
                      name="username"
                      className="mdl-textfield__input"
                      type="text"
                      value={username}
                      autoComplete="off"
                      onChange={this.onInputChange} />
                    <label htmlFor="username" className={(this.state.username ? 'label-up' : '')}>Username</label>
                  </span>
                </div>
                <div className="input-group">
                  <FontAwesome.FaLock className="icon" />
                  <span className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input
                      id="password"
                      name="password"
                      className="mdl-textfield__input"
                      type="password"
                      value={password}
                      autoComplete="off"
                      onChange={this.onInputChange} />
                    <label htmlFor="password" className={(this.state.password ? 'label-up' : '')}>Password</label>
                  </span>
                </div>
              </div>
              <div className="input-group submit">
                <button className="ui-button" disabled={!this.state.password}>
                  <span>LOGIN</span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { loginErrorMessage } = this.props;
    const { login } = this.state;

    const App = require('./app').default;
    if (login) {
      return (<App />);
    }
    // return this.loginPageRender(loginErrorMessage);
    return this.loginPageRender(loginErrorMessage);
  }
}

function mapStateToProps() {
  return {};
}


export default connect(mapStateToProps)(Login);
