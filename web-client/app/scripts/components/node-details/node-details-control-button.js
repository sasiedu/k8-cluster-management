import React from 'react';
import { connect } from 'react-redux';

import { trackAnalyticsEvent } from '../../utils/tracking-utils';
import { doControl } from '../../actions/app-actions';

function isUserAllowedTheButton(buttonTitle, user) {
  if (buttonTitle === 'Attach') {
    return true;
  }
  if (user.username === 'write') {
    return true;
  }
  return false;
}

class NodeDetailsControlButton extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {show: false};
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    const user = JSON.parse(localStorage.getItem('user'));
    const self = this;
    const isButtonAllowed = isUserAllowedTheButton(this.props.control.human, user);
    self.setState({show: isButtonAllowed});
  }

  render() {
    let className = `tour-step-anchor node-control-button fa ${this.props.control.icon}`;
    if (this.props.pending) {
      className += ' node-control-button-pending';
    }
    if (!this.state.show) {
      return (null);
    }
    return (
      <span className={className} title={this.props.control.human} onClick={this.handleClick} />
    );
  }

  handleClick(ev) {
    ev.preventDefault();
    const { id, human } = this.props.control;
    trackAnalyticsEvent('scope.node.control.click', { id, title: human });
    this.props.dispatch(doControl(this.props.nodeId, this.props.control));
  }
}

// Using this instead of PureComponent because of props.dispatch
export default connect()(NodeDetailsControlButton);
